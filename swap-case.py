def swap_case(s):
    ans = ""
    for letter in s:
        if letter == letter.upper():
            ans = ans + letter.lower()
        else:
            ans = ans + letter.upper()
    return ans
